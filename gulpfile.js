var gulp = require('gulp');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

function compileCSS() {
    return gulp
        .src([
            './resources/stylus/common.styl',
            '!./resources/stylus/**/_*.styl',
        ])
        .pipe(stylus({
            compress: true
        }))
        .pipe(gulp.dest('./public/css'));
};

function compileJS() {
    return gulp
        .src([
            './resources/js/strict.js',
            './resources/js/vars.js',
            './resources/js/funcs/**/*.js',
            './resources/js/common.js',
            '!./resources/js/**/_*.js',
        ])
        .pipe(uglify())
        .pipe(concat('common.js'))
        .pipe(gulp.dest('./public/js'));
};

gulp.task('css', compileCSS);
gulp.task('js', compileJS);

gulp.task('default', gulp.parallel(compileCSS, compileJS));

gulp.task('watch', function () {
    gulp.watch('./resources/stylus/**/*.styl', compileCSS);
    gulp.watch('./resources/js/**/*.js', compileJS);
});
